# Installing VS Code on Debian/Ubuntu


There are so many options, 
but my choice for the "programmer's editor" (or, a light-weight IDE) is Visual Studio Code these days:

    wget -c https://go.microsoft.com/fwlink/?LinkID=760868
    sudo dpkg -i vsc.deb

